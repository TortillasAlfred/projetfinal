#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import sys
import load_datasets
import time
import NeuralNet  # importer la classe du Réseau de Neurones
# import DecisionTree  # importer la classe de l'Arbre de Décision
# importer d'autres fichiers et classes si vous en avez développés
# importer d'autres bibliothèques au besoin, sauf celles qui font du machine learning

# Initializer vos paramètres
train_ratio = 0.7
print("Les datasets Iris et Congress seront traités avec un ratio d'entraînement de " + '{:0.2f}'.format(train_ratio * 100) + " %.\n")

# Initializer/instanciez vos classifieurs avec leurs paramètres
NN_Congress = NeuralNet.NeuralNet(2)
NN_Monk1 = NeuralNet.NeuralNet(2)
NN_Monk2 = NeuralNet.NeuralNet(2)
NN_Monk3 = NeuralNet.NeuralNet(2)
NN_Iris = NeuralNet.NeuralNet(3, iris=True)

# Charger/lire les datasets
"""
Les jeux de données sont standardisés pour le réseau de neurones
"""
[train_monk1, train_labels_monk1, test_monk1, test_labels_monk1] = load_datasets.load_monks_dataset(1)
[train_monk1_std, train_labels_monk1_std, test_monk1_std, test_labels_monk1_std] = load_datasets.load_monks_dataset(1, True)
[train_monk2, train_labels_monk2, test_monk2, test_labels_monk2] = load_datasets.load_monks_dataset(2)
[train_monk2_std, train_labels_monk2_std, test_monk2_std, test_labels_monk2_std] = load_datasets.load_monks_dataset(2, True)
[train_monk3, train_labels_monk3, test_monk3, test_labels_monk3] = load_datasets.load_monks_dataset(3)
[train_monk3_std, train_labels_monk3_std, test_monk3_std, test_labels_monk3_std] = load_datasets.load_monks_dataset(3, True)
[train_iris, train_labels_iris, test_iris, test_labels_iris] = load_datasets.load_iris_dataset(train_ratio)
[train_iris_std, train_labels_iris_std, test_iris_std, test_labels_iris_std] = load_datasets.load_iris_dataset(train_ratio, True)
[train_congress, train_labels_congress, test_congress, test_labels_congress] = load_datasets.load_congressional_dataset(train_ratio)
[train_congress_std, train_labels_congress_std, test_congress_std, test_labels_congress_std] = load_datasets.load_congressional_dataset(train_ratio, True)

# Entrainez votre classifieur
"""
Section pour la crossValidation. Un exemple est commenté pour la validation croisée
de Iris. Les résultats obtenus sont contenus dans les variables neurones{dataset}.
"""
print("CrossValidation")
# [erreurs, erreursTrain] = NN_Iris.crossValidate(train_iris_std, train_labels_iris_std)
# handle1, = plt.plot(erreurs.keys(), erreurs.values(), 'r', linewidth=1.5, label='Donnees tests')
# handle2, = plt.plot(erreursTrain.keys(), erreursTrain.values(), 'b', linewidth=1.5, label='Donnees train')
# plt.legend(handles=[handle1, handle2])
# plt.title("Iris")
# plt.ylabel('Erreur moyenne')
# plt.xlabel('Nombre de neurones dans la couche cachee')
# plt.show()
neuronesCongress = 11
neuronesIris = 40
neuronesMonk1 = 7
neuronesMonk2 = 11
neuronesMonk3 = 14
# neuronesIris = min(erreurs, key=erreurs.get)
print("*****RESULTATS CROSSVALIDATION (nbNeurones)*******")
print("Congress : " + str(neuronesCongress))
print("Iris : " + str(neuronesIris))
print("Monk1 : " + str(neuronesMonk1))
print("Monk2 : " + str(neuronesMonk2))
print("Monk3 : " + str(neuronesMonk3))

"""
Nombre de couches cachées. Exemple commenté pour Iris. 
Les résultats sont contenus dans les variables couches{dataset}
"""
# accuracy = []
# train = train_iris_std
# train_labels = train_labels_iris_std
# test = test_iris_std
# test_labels = test_labels_iris_std
# neurones = neuronesIris
# NN = NN_Iris
# titre = "Iris"
# for i in range(1, 6) :
#     print("DÉBUT D'ITERATION")
#     print(i)
#     accuracy.append({})
#     for sub in range(1, len(train)):
#         print("Nombre de donnees : " + str(sub))
#         indices = np.random.randint(0, train.shape[0], sub)
#         NN.train(train[indices], np.transpose(np.transpose(train_labels)[indices]), i, neurones)
#         accuracy[i-1][sub] = NN.test(test, test_labels, True)
# handlesPerso = []
# for i in range(0, len(accuracy)) :
#     temp, = plt.plot(accuracy[i].keys(), accuracy[i].values(), linewidth=1.5, label="RN-" + str(3+i))
#     handlesPerso.append(temp)
# plt.legend(handles=handlesPerso)
# plt.title(titre)
# plt.ylabel('Precision sur le jeu de test')
# plt.xlabel('Nombre de donnees dans le training')
# plt.show()
couchesCongress = 2
couchesMonk1 = 1
couchesMonk2 = 4
couchesMonk3 = 3
couchesIris = 1
print("*****RESULTATS NOMBRE DE COUCHES*******")
print("Congress : " + str(couchesCongress))
print("Iris : " + str(couchesIris))
print("Monk1 : " + str(couchesMonk1))
print("Monk2 : " + str(couchesMonk2))
print("Monk3 : " + str(couchesMonk3))


"""
Impact de l'initialisation des poids
"""
# accuracy = []
# train = train_iris_std
# train_labels = train_labels_iris_std
# test = test_iris_std
# test_labels = test_labels_iris_std
# neurones = neuronesIris
# nbCouches = couchesIris
# NN = NN_Iris
# titre = "Iris"
# for i in range(0, 2) :
#     print("DÉBUT D'ITERATION")
#     print(i)
#     accuracy.append({})
#     poidsZero = (i == 0)
#     for sub in range(1, len(train)):
#         print("Nombre de donnees : " + str(sub))
#         indices = np.random.randint(0, train.shape[0], sub)
#         NN.train(train[indices], np.transpose(np.transpose(train_labels)[indices]), nbCouches, neurones, poidsZero)
#         accuracy[i][sub] = NN.test(test, test_labels, True)
# handleRNN_zero, = plt.plot(accuracy[0].keys(), accuracy[0].values(), linewidth=1.5, label="RN-ZERO")
# handelRNN_NonZero, = plt.plot(accuracy[1].keys(), accuracy[1].values(), linewidth=1.5, label="RN-NON-ZERO")
# plt.legend(handles=[handleRNN_zero, handelRNN_NonZero])
# plt.title(titre)
# plt.ylabel('Precision sur le jeu de test')
# plt.xlabel('Nombre de donnees dans le training')
# plt.show()

# print("TRAINING")
start_time = time.clock()
NN_Congress.train(train_congress_std,train_labels_congress_std, couchesCongress, neuronesCongress)
print("Temps d'execution entrainement Congress : " + '{:0.2f}'.format(time.clock() - start_time) + " secondes.\n")
start_time = time.clock()
NN_Monk1.train(train_monk1_std, train_labels_monk1_std, couchesMonk1, neuronesMonk1)
print("Temps d'execution entrainement Monk1 : " + '{:0.2f}'.format(time.clock() - start_time) + " secondes.\n")
start_time = time.clock()
NN_Monk2.train(train_monk2_std, train_labels_monk2_std, couchesMonk2, neuronesMonk2)
print("Temps d'execution entrainement Monk2 : " + '{:0.2f}'.format(time.clock() - start_time) + " secondes.\n")
start_time = time.clock()
NN_Monk3.train(train_monk3_std, train_labels_monk3_std, couchesMonk3, neuronesMonk3)
print("Temps d'execution entrainement Monk3 : " + '{:0.2f}'.format(time.clock() - start_time) + " secondes.\n")
start_time = time.clock()
NN_Iris.train(train_iris_std, train_labels_iris_std, couchesIris, neuronesIris)
print("Temps d'execution entrainement Iris : " + '{:0.2f}'.format(time.clock() - start_time) + " secondes.\n")
start_time = time.clock()

print("TEST avec donnees de training")
# Tester votre classifieur
print("*****CONGRESS********")
NN_Congress.test(train_congress_std, train_labels_congress_std)
print("*****MONK1***********")
NN_Monk1.test(train_monk1_std, train_labels_monk1_std)
print("*****MONK2***********")
NN_Monk2.test(train_monk2_std, train_labels_monk2_std)
print("*****MONK3***********")
NN_Monk3.test(train_monk3_std, train_labels_monk3_std)
print("*****IRIS************")
NN_Iris.test(train_iris_std, train_labels_iris_std)


print("TEST avec donnees de test")
print("*****CONGRESS******")
NN_Congress.test(test_congress_std, test_labels_congress_std)
print("*****MONK1***********")
NN_Monk1.test(test_monk1_std, test_labels_monk1_std)
print("*****MONK2***********")
NN_Monk2.test(test_monk2_std, test_labels_monk2_std)
print("*****MONK3***********")
NN_Monk3.test(test_monk3_std, test_labels_monk3_std)
print("*****IRIS************")
NN_Iris.test(test_iris_std, test_labels_iris_std)