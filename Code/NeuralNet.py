#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Vous allez definir une classe pour chaque algorithme que vous allez développer,
votre classe doit contenit au moins les 3 methodes definies ici bas,
	* train 	: pour entrainer le modèle sur l'ensemble d'entrainement
	* predict 	: pour prédire la classe d'un exemple donné
	* test 		: pour tester sur l'ensemble de test
vous pouvez rajouter d'autres méthodes qui peuvent vous etre utiles, mais moi
je vais avoir besoin de tester les méthodes train, predict et test de votre code.
"""

import numpy as np
import math
import random
import time

# le nom de votre classe
# NeuralNet pour le modèle Réseaux de Neurones
# DecisionTree le modèle des arbres de decision

class NeuralNet: #nom de la class à changer

	def __init__(self, nbOutput, **kwargs):
		"""
		c'est un Initializer.
		Vous pouvez passer d'autre paramètres au besoin,
		c'est à vous d'utiliser vos propres notations
		"""
		self.nbOutput = nbOutput
		self.alpha = 0.01
		if 'iris' in kwargs.keys():
			self.Iris = kwargs['iris']
		else:
			self.Iris = False


	def train(self, train, train_labels, nbCouches, neuronesParCouche, poidsZero = False): #vous pouvez rajouter d'autres attribus au besoin
		assert(nbCouches >  0 and neuronesParCouche > 0)

		# Erreur moyenne tolérée sur chaque donnée 
		epsilon = 10**-1

		self.poids = {}
		self.couches = []
		self.noeuds = {}
		nbInput = train.shape[1]
		
		# Implémentation de back-prop-learning
		# On bâtit la forme du réseau

		# Entrée
		self.couches.append([i for i in range(0, nbInput)])

		# Première couche cachée
		couche = []
		for neurone in range(0, neuronesParCouche):
			j = nbInput + neurone
			couche.append(j)
			for i in range(0, nbInput):
				self.ajouterPoids(i, j, True)
		self.couches.append(couche)

		# Couches cachées supplémentaires
		for indexCouche in range(1, nbCouches) :
			couche = []
			for neurone in range(0, neuronesParCouche):
				j = nbInput + neurone + indexCouche * (neuronesParCouche)
				couche.append(j)
				for indexNeurone in range(0, neuronesParCouche) :
					i = nbInput + (indexCouche - 1) * neuronesParCouche + indexNeurone
					self.ajouterPoids(i,j)
			self.couches.append(couche)


		# Couche de sortie
		coucheFinale = []
		for index in range(0, self.nbOutput):
			j = nbInput + index + nbCouches * neuronesParCouche
			coucheFinale.append(j)
			for neurone in range(0, neuronesParCouche) :
				i = nbInput + (nbCouches - 1) * neuronesParCouche + neurone
				self.ajouterPoids(i, j)
		self.couches.append(coucheFinale)

		if not poidsZero :
			self.initPoids(nbInput, self.nbOutput, neuronesParCouche, nbCouches)

		np.random.seed(1)
		iter = 0
		epoch = 0
		erreur = 0.0
		while True:
			epoch += 1
			erreurs = []
			indices = np.arange(train.shape[0])
			np.random.shuffle(indices)
			train = train[indices]
			transposee = np.transpose(train_labels)
			transposee = transposee[indices]
			train_labels = np.transpose(transposee)

			for i in range(0, len(train)):
				iter += 1
				a = {}
				a[-1] = 1.0
				input = {}
				delta = {}
				target = [0 for ind in range(0, self.nbOutput)]
				target[train_labels.item(i)] = 1
				exemple = train[i]

				# Forward
				for j in range(0, len(self.couches[0])):
					a[j] = exemple.item(j)

				for l in range(1, len(self.couches)):
					for j in self.couches[l]:
						somme = 0.0
						for i in self.noeuds[j][0]:
							somme += self.poids[(i, j)] * a[i]
						input[j] = somme
						a[j] = g(input[j])

				# Backward
				for noeud in range(0, self.nbOutput) :
					j = coucheFinale[noeud]
					delta[j] = derivG(input[j]) * (target[noeud] - a[j])

				for l in range(len(self.couches) - 2, 0, -1) :
					for i in self.couches[l] :
						somme = 0.0
						for j in self.noeuds[i][1] :
							somme += self.poids[(i,j)] * delta[j]
						delta[i] = derivG(input[i]) * somme

				# Weight update
				for cle in self.poids.keys() :
					i = cle[0]
					j = cle[1]
					self.poids[cle] += self.alpha * a[i] * delta[j]

				# Erreur quadratique
				erreurs.append(sum((target[i - coucheFinale[0]] - a[i])**2 for i in coucheFinale))

			erreurPrec = erreur
			erreur = sum(erreurs)/2

			# Critère d'arrêt hybride sur la première occurence entre la 
			# convergence de l'erreur totale sur le jeu de données et
			# l'atteinte de la 251ème époque
			if erreur < epsilon * len(train) or epoch > 250 :
				break

	def predict(self, exemple):
		# Simplement une implémentation de forward.
		# Le réseau retourne le noeud avec la plus grande valeur sur la couche de sortie
		a = {}
		a[-1] = 1
		input = {}

		for j in range(0, len(self.couches[0])):
			a[j] = exemple.item(j)

		for l in range(1, len(self.couches)):
			for j in self.couches[l]:
				somme = 0.0
				for i in self.noeuds[j][0]:
					somme += self.poids[(i, j)] * a[i]
				input[j] = somme
				a[j] = g(input[j])

		prediction = [a[i] for i in self.couches[len(self.couches) - 1]]

		return prediction.index(max(prediction))

	def test(self, test, test_labels, cross=False):
		compteurTotal = len(test)

		n = 3 if (self.Iris) else 2
		confusion = np.zeros(shape=(n,n))

		start_time = time.clock()
		total_time = 0.0

		# On bâtit la matrice de confusion
		for i in range(0, compteurTotal):
			data = test[i]
			etiquette = test_labels.item(i)
			start_time = time.clock()
			predict = self.predict(data)
			total_time += time.clock() - start_time
			confusion[predict, etiquette] += 1

		meanPredictTime = total_time*1000.0/compteurTotal

		print("Temps moyen de prediction d'un seul exemple : " + '{:0.6f}'.format(meanPredictTime) + " ms.")

		if cross :
			return sum([confusion[i][i] for i in xrange(n)])/float(compteurTotal) * 100#accuracy
		else :
			if (not self.Iris) :
				printConfusion(confusion, compteurTotal)
			else :
				# Séparation en 3 matrices de un-contre-tous pour le cas Iris
				print("Matrice de confusion incluant tous les cas\n")
				print(confusion.astype(int))
				print("\n")
				# On doit faire sortir 3 matrices de confusion dans le cas Iris
				for i in range(0, n):
					confusion_i = np.zeros(shape=(2,2))
					print("\nMatrice pour le cas " + str(i))
					# vraiPos
					confusion_i[0, 0] = confusion[i, i]
					# vraiNeg
					confusion_i[1, 1] = confusion[(i + 1) % n, (i + 1) % n] + confusion[(i + 1) % n, (i + 2) % n] + confusion[(i + 2) % n, (i + 1) % n] + confusion[(i + 2) % n, (i + 2) % n]
					# fauxPos
					confusion_i[0, 1] = confusion[i, (i + 1) % n] + confusion[i, (i + 2) % n]
					# faux Neg
					confusion_i[1, 0] = confusion[(i + 1) % n, i] + confusion[(i + 2) % n, i]

					printConfusion(confusion_i, compteurTotal)


	def ajouterPoids(self, i, j, input=False) :
		# On ajoute les poids, tous initialisés à zéro pour le moment.
		# i est le noeud départ, j le noeud destination.
		# On s'assure aussi que si on ne connaît pas déjà i ou j, on l'ajoute à la liste des noeuds
		# en le connectant au biais de 1.0 (et en créant le poids reliant les deux) si le noeud 
		# n'est pas sur la coucheInitiale (input = True)
		self.poids[(i,j)] = 0.0

		if not self.noeuds.has_key(i) :
			self.noeuds[i] = [[],[]]
			if not input :
				self.noeuds[i][0].append(-1)
				self.poids[(-1, i)] = 1.0

		if not self.noeuds.has_key(j) :
			self.noeuds[j] = [[], []]
			self.noeuds[j][0].append(-1)
			self.poids[(-1, j)] = 1.0


		noeud_i = self.noeuds[i]
		noeud_j = self.noeuds[j]
		noeud_i[1].append(j)
		noeud_j[0].append(i)

	def initPoids(self, fan_in, fan_out, nbNeurones, nbCouches):
		# Initialisation de Xavier.
		# On utilise une distribution normale de moyenne 0 et de variance
		# 2/(nombre d'entrées du noeud)
		np.random.seed(1)
		var = 2.0/fan_in
		distribution = np.random.normal(0, var, (nbNeurones + 1) * fan_in)
		j=0
		for i in range(0, len(distribution)):
			cle = self.poids.keys()[j]
			self.poids[cle] = distribution[j]
			j+=1
		var = 2.0/nbNeurones
		distribution = np.random.normal(0, var, len(self.poids) - (nbNeurones + 1) * fan_in)
		for i in range(0, len(distribution)) :
			cle = self.poids.keys()[j]
			self.poids[cle] = distribution[i]
			j+=1

	def crossValidate(self, train, train_labels) :
		# Validation croisée.
		k = 5 # choix arbitraire
		nbItemsACouper = len(train)%k

		np.random.seed(1)
		for i in range(0, nbItemsACouper) :
			n = random.randint(0, len(train))
			train = np.delete(train, (n), axis=0)
			train_labels = np.delete(train_labels, (n), axis=1)

		tranches = len(train)/k
		folds = []
		folds_labels = []
		for i in range(0, k) :
			folds.append(train[tranches * i : tranches * (i + 1), :])
			folds_labels.append(train_labels[0, tranches * i : tranches * (i + 1)])

		erreurs = {}
		erreursTrain = {}
		for nbNeurones in range(4,51) :
			print("*****TEST AVEC " + str(nbNeurones) + " neurones. ********")
			erreurTotale = 0.0
			erreurTotaleTrain = 0.0
			for i in range(0, k) :
				print("ENTRAINEMENT NUMERO " + str(i+1))
				data_train = np.concatenate([folds[index] for index in range(0, k) if index != i], axis=0)
				labels_train = np.concatenate([folds_labels[index] for index in range(0, k) if index != i], axis=1)
				data_test = folds[index]
				labels_test = folds_labels[index]
				self.train(data_train, labels_train, 1, nbNeurones)
				erreur = (100.0 - self.test(data_test, labels_test, True))/100
				erreurTrain = (100.0 - self.test(data_train, labels_train, True))/100
				erreurTotale += erreur
				erreurTotaleTrain += erreurTrain
			erreurs[nbNeurones] = erreurTotale/k
			erreursTrain[nbNeurones] = erreurTotaleTrain/k
		print(erreurs)
		print(erreursTrain)
		return [erreurs, erreursTrain]


def printConfusion(confusion, compteurTotal) :
	# matrice de confusion
	print("Matrice de confusion\nLes colonnes sont les valeurs reelles, les lignes sont les valeurs predites.\n")
	print(confusion.astype(int))
	print("\n")

	# (Vrais Positifs + Vrais Négatifs) / (Total)
	accuracy = (confusion[0][0] + confusion[1][1])/float(compteurTotal) * 100
	print("Accuracy : " + '{:0.2f}'.format(accuracy) + " %")


# Vous pouvez rajouter d'autres méthodes et fonctions,
# il suffit juste de les commenter.

"""
La fonction d'activation. On a choisi leaky ReLu avec une constante a = 0.01
"""
def g(valeur):
	return max(0.01 * valeur, valeur)

"""
La dérivée de la fonction d'activation.
"""
def derivG(valeur):
	if valeur < 0.0 :
		return 0.01
	else :
		return 1.0