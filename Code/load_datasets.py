#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import random
import math

def load_iris_dataset(train_ratio, standardization = False):
    random.seed(1) # Pour avoir les meme nombres aléatoires à chaque initialisation.
    
    # Vous pouvez utiliser des valeurs numériques pour les différents types de classes, tel que :
    conversion_labels = {'Iris-setosa': 0, 'Iris-versicolor' : 1, 'Iris-virginica' : 2}
    
    # Le fichier du dataset est dans le dossier datasets en attaché 
    f = open('datasets/bezdekIris.data', 'r')
    
    
    # TODO : le code ici pour lire le dataset
    
    # REMARQUE très importante : 
	# remarquez bien comment les exemples sont ordonnés dans 
    # le fichier du dataset, ils sont ordonnés par type de fleur, cela veut dire que 
    # si vous lisez les exemples dans cet ordre et que si par exemple votre ration est de 60%,
    # vous n'allez avoir aucun exemple du type Iris-virginica pour l'entrainement, pensez
    # donc à utiliser la fonction random.shuffle pour melanger les exemples du dataset avant de séparer
    # en train et test.

    assert(train_ratio <= 1 and train_ratio >= 0)

    lines = f.readlines()
    contentString = [line.strip().split(',') for line in lines if line != '\n']
    # On traduit les strings en float pour les 3 premieres champs
    content = [[float(data[0]), float(data[1]), float(data[2]), float(data[3]), conversion_labels[data[4]]] for data in contentString]
    random.shuffle(content)
    tailleData = len(content) - 1

    assert(tailleData > 0)

    beginTrain = 0
    endTrain = int(math.floor(tailleData * train_ratio))
    beginTest = endTrain
    endTest = tailleData

    if standardization :
        mean = []
        std_dev = []
        for i in range(0, 4) :
            total = sum([d[i] for d in content])
            mean.append(total/len(content))
            std_dev.append((sum([(d[i] - mean[i])**2 for d in content])/(len(content)))**(1.0/2))

        train = np.matrix([[(d[i] - mean[i])/std_dev[i] for i in range(0,4)] for d in content[beginTrain: endTrain]])
        train_labels = np.matrix([d[4] for d in content[beginTrain: endTrain]])
        test = np.matrix([[(d[i] - mean[i])/std_dev[i] for i in range(0,4)] for d in content[beginTest: endTest]])
        test_labels = np.matrix([d[4] for d in content[beginTest: endTest]])
    else :
        train = np.matrix([d[0:4] for d in content[beginTrain: endTrain]])
        train_labels = np.matrix([d[4] for d in content[beginTrain: endTrain]])
        test = np.matrix([d[0:4] for d in content[beginTest: endTest]])
        test_labels = np.matrix([d[4] for d in content[beginTest: endTest]])
    
    # Tres important : la fonction doit retourner 4 matrices (ou vecteurs) de type Numpy. 
    return (train, train_labels, test, test_labels)
	
	
	
def load_congressional_dataset(train_ratio, standardization = False):    
    random.seed(1) # Pour avoir les meme nombres aléatoires à chaque initialisation.
    
    # Vous pouvez utiliser un dictionnaire pour convertir les attributs en numériques 
    # Notez bien qu'on a traduit le symbole "?" pour une valeur numérique
    # Vous pouvez biensur utiliser d'autres valeurs pour ces attributs
    # 
    # NB : On a inversé la position de '?' et 'y' pour réfléter qu'un '?'
    # est aussi proche d'un oui que d'un non
    conversion_labels = {'republican' : 0, 'democrat' : 1, 
                         'n' : 0, '?' : 1, 'y' : 2} 
    
    # Le fichier du dataset est dans le dossier datasets en attaché 
    f = open('datasets/house-votes-84.data', 'r')

	
    # TODO : le code ici pour lire le dataset
    assert(train_ratio <= 1 and train_ratio >= 0)

    lines = f.readlines()
    contentString = [line.strip().split(',') for line in lines if line != '\n']
    content = [[conversion_labels[data[i]] for i in range(0,17)] for data in contentString]
    random.shuffle(content)
    tailleData = len(content) - 1

    assert(tailleData > 0)

    beginTrain = 0
    endTrain = int(math.floor(tailleData * train_ratio))
    beginTest = endTrain
    endTest = tailleData

    if standardization :
        mean = {}
        std_dev = {}
        for i in range(1,17) :
            total = sum([d[i] for d in content])
            mean[i] = float(total)/len(content)
            std_dev[i] = (sum([(d[i] - mean[i])**2 for d in content])/(len(content)))**(1.0/2)

        train = np.matrix([[(d[i] - mean[i])/std_dev[i] for i in range(1,17)] for d in content[beginTrain: endTrain]])
        train_labels = np.matrix([d[0] for d in content[beginTrain: endTrain]])
        test = np.matrix([[(d[i] - mean[i])/std_dev[i] for i in range(1,17)] for d in content[beginTest: endTest]])
        test_labels = np.matrix([d[0] for d in content[beginTest: endTest]])
    else :
        train = np.matrix([[d[i] for i in range(1,17)] for d in content[beginTrain:endTrain]])
        train_labels = np.matrix([i[0] for i in content[beginTrain:endTrain]])
        test = np.matrix([[d[i] for i in range(1,17)] for d in content[beginTest:endTest]])
        test_labels = np.matrix([i[0] for i in content[beginTest:endTest]])

    return (train, train_labels, test, test_labels)


def load_monks_dataset(numero_dataset, standardization = False):
	# TODO : votre code ici, vous devez lire les fichiers .train et .test selon l'argument numero_dataset
     
    assert(numero_dataset >= 1 and numero_dataset <= 3)

    num = str(numero_dataset)

    # Le fichier du dataset est dans le dossier datasets en attaché 
    f_test = open('datasets/monks-' + num + '.test', 'r')
    f_train = open('datasets/monks-' + num + '.train', 'r')

    lines_test = f_test.readlines()
    lines_train = f_train.readlines()
    contentString_test = [line.strip().split(' ') for line in lines_test if line != '\n']
    contentString_train = [line.strip().split(' ') for line in lines_train if line != '\n']
    # On traduit les strings en int pour les 7 premiers attributs
    content_test = [[int(data[0]),int(data[1]),int(data[2]),int(data[3]),int(data[4]),int(data[5]),int(data[6])] for data in contentString_test]
    content_train = [[int(data[0]),int(data[1]),int(data[2]),int(data[3]),int(data[4]),int(data[5]),int(data[6])] for data in contentString_train]
    random.shuffle(content_test)
    random.shuffle(content_train)
    tailleData_test = len(content_test) - 1
    tailleData_train = len(content_train) - 1

    assert(tailleData_test > 0)
    assert(tailleData_train > 0)

    if standardization :
        mean = {}
        std_dev = {}
        for i in range(1, 7) :
            total = sum([d[i] for d in content_train])
            mean[i] = float(total)/len(content_train)
            std_dev[i] = (sum([(d[i] - mean[i])**2 for d in content_train])/(len(content_train)))**(1.0/2)

        train = np.matrix([[(d[i] - mean[i])/std_dev[i] for i in range(1,7)] for d in content_train])
        train_labels = np.matrix([d[0] for d in content_train])
        test = np.matrix([[(d[i] - mean[i])/std_dev[i] for i in range(1,7)] for d in content_test])
        test_labels = np.matrix([d[0] for d in content_test])
    else :
        train = np.matrix([d[1:7] for d in content_train])
        train_labels = np.matrix([d[0] for d in content_train])
        test = np.matrix([d[1:7] for d in content_test])
        test_labels = np.matrix([d[0] for d in content_test])

    # La fonction doit retourner 4 matrices (ou vecteurs) de type Numpy. 
    return (train, train_labels, test, test_labels)